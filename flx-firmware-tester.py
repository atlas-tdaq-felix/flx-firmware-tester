#!/usr/bin/env python

import os
import sys
import json
import time
import socket

from string import *
from optparse import OptionParser

import locale
from locale import atof

parser = OptionParser()
parser.add_option("-f", "--file", 
                  action="store", type="string", dest="cfilename", default="tests.dat",
                  help="read config from FILE", metavar="FILE")
parser.add_option("-l", "--list",
                  action="store_true", dest="list", default=False,
                  help="print tests to stdout")
parser.add_option("-v", "--verbose", action="store", type="int", dest="level", default=0,
                  help="print debug messages to stdout")
parser.add_option("-d", "--card", action="store", type="int", dest="card", default=0,
                  help="select card to test")
parser.add_option("-w", "--firmware", 
                  action="store", type="string", dest="firmware", default="XXYYZZ",
                  help="the target firmware for testing", metavar="FILE")
parser.add_option("-x", "--hardware",
                  action="store", type="string", dest="hardware", default="FLX-709",
                  help="the target hardware for testing", metavar="FILE")

(options, args) = parser.parse_args()

swtag=os.popen( "git describe --tag --always" ).readlines() 
#swtag="felix-04-00-05-40-g40d9b82"
fwtag=options.firmware
hwtype=options.hardware
card=options.card
cfilename=options.cfilename
verbose_level=options.level 
print_test_list=options.list


### path is ../BINARY_TAG/
#prepath="../" + os.environ["BINARY_TAG"] + "/"
prepath=" "
empty=" "

#print cfilename, verbose_level, print_test_list

f = open(cfilename)

testlist=[]
atest=[1,2,[],1]
acond=[1,2,"NONE","=",0]
new_cmd=False
new_condition=False

for dummy in f.readlines():
   if len(dummy) > 5 :
     if dummy[0] != "#" :
        abc=dummy.split(":",1);
###       print  dummy, "]"
        new_abc=abc[1].replace("CARDID", str(card))
        new_abc=new_abc.replace("CBRDID", str(card+1))
        abc[1]=new_abc
        if abc[0].strip() == "Cmd" :
            atest[0]=abc[1].strip()
            if (verbose_level >1) : print ("Cmd found")
            new_cmd=True
        elif abc[0].strip() == "Name" :
            atest[1]=abc[1].strip()
            if (verbose_level >1) : print ("Name found")
        elif abc[0].strip() == "Rep" :
            atest[3]=abc[1].strip()
            if (verbose_level >1) : print ("Repetitionfound")
        elif abc[0].strip() == "Pass" :
##########add a new condition here
            if new_condition==True :
               atest[2].append(acond)
               acond=[1,2,"NONE","=",0]
               new_condition=False
            acond[0]=abc[1].strip()
            signlocation=acond[0].find('>')
            if (signlocation < 0 ) :
              signlocation=acond[0].find('<')
              if ( signlocation >0 ) :
                 acond[3]="<"
            else:
              acond[3]=">"

            if ( signlocation >0 ) :
              avalue=acond[0][signlocation+1:]
              acond[0]=acond[0][:signlocation]
              acond[4]=avalue
#             print (acond[0], " 1:", acond[1], " 2:", acond[2], " v:",avalue, "\n")
            if (verbose_level >1) : print ("Pass found")
        elif abc[0].strip() == "Err" :
            acond[1]=abc[1].strip()
            new_condition=True 
            if (verbose_level >0) : print ("Error found")
        elif abc[0].strip() == "Action" :
            acond[2]=abc[1].strip()
            new_condition=True 
            if (verbose_level >0) : print ("Action found")
   else :
     if new_cmd == True :
         if new_condition==True :
            atest[2].append(acond)
            acond=[1,2,"NONE","=",0]
            new_condition=False
         testlist.append(atest)
         atest=[1,2,[],1]
         new_cmd=False;

if print_test_list == True :
   for ctest in testlist:
      print (ctest[0], ctest[1],"      REP:", ctest[3])
      print (ctest[2],"\n")
#######################################################

have_a_failure=False
#######################################################
##################  EXECUTION  ########################
#######################################################

outf = open('outfile.json', 'w')
outf.write('{  "reportName" : "Felix nightly tests",')
outf.write(' "workArea" : "CERN", ')
outf.write(' "testDate" : "')
##outf.write(time.strftime("%d/%m/%Y") )
outf.write(time.strftime("%Y-%m-%d") )
outf.write('", ')
outf.write(' "active" : true, ')
outf.write(' "members" : [ { \n')

compname=socket.gethostname()
outf.write(' "swTag" : "')
outf.write(swtag[0].strip())
outf.write('", "fwTag" : "')
outf.write(fwtag)
outf.write('", "hwType" : "')
outf.write(hwtype)
outf.write('", "computerID" : "')
outf.write(compname)
outf.write('", ')

success_list={}
fails_list={}
ignores_list={}
all_succ_list=[]
all_fail_list=[]
all_igno_list=[]
atestname=""

for ctest in testlist:
  for repe in range(0, int((ctest[3])) ) :
    should_be_repeated=False;
    retval = os.popen( ctest[0] ).readlines()
    print
    if ctest[1] == 2 :
      atestname=ctest[0] 
    else:
      atestname=ctest[1] 
    print (atestname)
    if (int((ctest[3])) > 1) :
      atestname+=" trial-"
      atestname+=str(repe)
    didonceI=0
    didonceS=0
    didonceF=0
    igno_list={}
    succ_list={}
    fail_list={}

# count for various error conditions
    err_count=0
    for ccond in ctest[2] :
      ccond_passed=False
      display_str=""
      if (verbose_level >= 1) :  print (retval, ccond[0])
      if ( len(ccond[0])  < 3) :
##### just a number comparison########
        if len(retval) >= atof(ccond[0]) : 
           ccond_passed=True 
      else :
####### this is a string comparison
        for rv in retval :
          if ccond[0] in rv :
             if (ccond[3] != "=" ):
              l1 = ccond[0].split()
              l2 = rv.split()
              s2 = ""
              for j in l2:
                if j not in l1:
                 if (j != ":" and j != "="):
                  s2 = s2 + " " + j 
              s2.strip()
#             print ("-------->",ccond[4],"vs",s2)
              if (ccond[3] == ">" ):
                if (ccond[4] < s2 ):
#                  print ("greater\n")
                   ccond_passed=True
                   display_str=rv.strip()
              else :
                if (ccond[4] > s2 ):
#                  print ("smaller\n")
                   ccond_passed=True
                   display_str=rv.strip()
             else :
              ccond_passed=True
              display_str=rv.strip()

#-------- print on screen        
      if ccond_passed == True :
         if (didonceS == 0) :
            success_list[atestname]=[]
            succ_list["output"]=[]
            succ_list["name"]=atestname
            didonceS=1
         ok_str="\t"
         if (len(display_str) >1) : 
##           print len(display_str)
           for kt in range(0, 5- (int)(len(display_str)/8)) :
              ok_str+="\t"
           ok_str+="OK"
           print (display_str, ok_str)
           success_list[atestname].append(display_str)
           succ_list["output"].append(display_str)
         else:
##           print len(ccond[0]) 
           for kt in range(0, 5- (int)(len(ccond[0]) / 8)) :
                ok_str+="\t"
           ok_str+="OK"
           print (ccond[0], ok_str)
           success_list[atestname].append(ccond[0])
           succ_list["output"].append(ccond[0])
      else :
# this is error case
# below is our own err message
           delim='`'
           execcond=ccond[1].find(delim)
           if ( execcond == 0 ) :
              newcmd=(ccond[1].split(delim))[1].split(delim)[0]
              print ("exec:",newcmd)
              errval = os.popen( newcmd ).readlines()
              ccond[1]=errval
           print (ccond[1])

# below is the output from terminal printed on screen
#          print (empty.join(retval) )
#           print "here" , didonceF , and didonceS==0, ccond[2]
           if (didonceF == 0 and ccond[2] != "SKIP" ) :
#              print "initialize variables"
              fails_list[atestname]=[]
              fail_list["output"]=[]
              fail_list["name"]=atestname
              didonceF=1
####### warning case is not an error
           if ("WARNING" in ccond[1]): 
              ccond_passed = True
              if (didonceS > 0) : 
                  success_list[atestname].append(ccond[1])
                  succ_list["output"].append(ccond[1])
              if (didonceF > 0) : 
                  fails_list[atestname].append(ccond[1])
                  fail_list["output"].append(ccond[1])
           if ccond[2] == "STOP" :
              print ("Emergency STOP\n")
              if (didonceF > 0) : 
                  fails_list[atestname].append(ccond[1])
                  fail_list["output"].append(ccond[1])
              exit (1)
           if ccond[2] == "SKIP" :
              if (didonceI > 0) : 
                  ignores_list[atestname].append(ccond[1])
                  igno_list["output"].append(ccond[1])
              else :
                  ignores_list[atestname]=[]
                  igno_list["output"]=[]
                  igno_list["output"].append(ccond[1])
                  igno_list["name"]=atestname
                  didonceI=1
                  
              ccond_passed = True
              break
           if ((ccond[2][0] == ".") & (repe<int((ctest[3])) )) :
              print ("__corrective action being taken__ @", repe)
              aretval = os.popen( ccond[2] ).readlines()
              should_be_repeated=True;
#-----check each condition
      if (ccond_passed != True): 
          err_count=err_count+1
          fails_list[atestname].append(ccond[1])
          fail_list["output"].append(ccond[1])
          fail_list["output"].append("\n");
          execution_output=empty.join(retval)
          if ( len (execution_output) > 50 ) :
              execution_output="too long to display!!"
          fail_list["output"].append(execution_output);
    if (len(igno_list)>0): all_igno_list.append(igno_list)
    if (len(succ_list)>0): all_succ_list.append(succ_list)
    if (len(fail_list)>0): all_fail_list.append(fail_list)
##  print "This test:", should_be_repeated
    if ( should_be_repeated == False ): 
      if (err_count > 0) : have_a_failure=True
      break


passsize=len(success_list)
failsize=len(fails_list)
ignoresize=len(ignores_list)
outf.write(' "noOfPassedTests": ')
outf.write(str(passsize))
outf.write(',"noOfFailedTests": ')
outf.write(str(failsize))
outf.write(',"noOfIgnoredTests": ')
outf.write(str(ignoresize))
outf.write(', "passedTests":  \n')
#json.dump(success_list, outf, separators=(',', ': '))
json.dump(all_succ_list, outf, separators=(',', ': '), indent = 4)
outf.write('\n ')
outf.write(',"failedTests" : ')
json.dump(  all_fail_list, outf, separators=(',', ': '), indent = 4)
outf.write(',"ignoredTests" : ')
json.dump(  all_igno_list, outf, separators=(',', ': '), indent = 4)

#print ("~~~~~~~~~~~~~~~~~~~~~~~~~")
#print (all_list)
#print ("~~~~~~~~~~~~~~~~~~~~~~~~~")

outf.write('\n },{ \n')
print ("ANY Failure?",have_a_failure)
