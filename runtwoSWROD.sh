#!/bin/bash
#export FI_PROVIDER=sockets
rm -f fileA fileB
sleep 1
mkfifo  /tmp/stats

ldd `which felix-tohost` > OUT1
echo "---" >> OUT1
ldd `which felix-test-swrod-unbuf` >> OUT1
echo $LD_LIBRARY_PATH >> OUT1

echo run $1
$1 2> Aerror.txt 1> fileA & 
sleep 1;
echo run $2
$2 2> Berror.txt 1> fileB &
sleep 1;

ps uxww  >> OUT1

sleep 45;

 kill %2
 kill -2 %1

cat fileA
cat Aerror.txt
cat fileB
cat Berror.txt



cat fileB  | grep "data rate:"
if [ $? -ne 0 ]; then
 tail -10 fileB  | grep kHz | cut -f3 -d',' | awk  '{sum += $2}  END {printf("Average kHz: %5.2f \n", sum/ NR) }'
else
 cat fileB  | grep "data rate:" | awk  '{ sum += $3} END {printf("Average: %5.2f \n", sum/ NR) }'
fi

