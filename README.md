This is theREADME file for the FELIX Tool to test firmware

1. This is the section on the tester python script

There are 2 related files
- tester.py : this is the main runner script. use -h to get help
- tests.dat : this is the example and default configuration file

The syntax in the configuration file is simple. There are 6 main
keywords: Name, Cmd, Pass, Err, Action, Rep 
Name   is the option string defining the current test. Should be human readable.
Cmd    is the command to be executed for the actual test. e.g. cat /proc/flx
         the BINARY_TAG is automatically added for the "f" tools.
Rep    is the number of times the corrective action & retest should be retried
         in case of an error.
Pass   is the condition to be satisfied for a successful test. It can be
         an integer to which the output length will be compared (> assumed) OR
         a string which will be searched in the output of the command
Err    is the error message to be printed to STDOUT in case of failures
Action is the action in case of failure, currently there is only 2 special actions
defined: 
--- STOP: it stops all further testing.
--- SKIP: it skips the current test.
--- any birary from "f" tools, the $BINARY_TAG is automatically added.

Any line starting with a # is ignored.

Note that each test can contain muliple Pass conditions and associated error 
messages which will be tried sequentially. Multiple Pass conditions should not
be separeted by blank lines. BUT
A different test must be defined after a blank line.
The configuration file must end with a blank line.

The default configuration file contains some test examples.
